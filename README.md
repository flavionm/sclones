# SCloneS - Scheduled rClone Synchronizer

SCloneS is a small collection of scripts designed to automate scheduled rclone bisyncs using systemd.

SCloneS is still in the very early stages, and should only be used for testing purposes.

## Dependencies

Rclone >= 1.66.0

Rclone executable is expected to be in the $PATH by default, but you can point to any other location by editing `RCLONE_COMMAND` in `sclones.sh`. Useful for Steam Deck users to install without admin privileges, just drop the executable somewhere and point to it.

## Installing

Installation and execution are (currently) done through user files in the home directory.

- Copy `sclones.sh`, `sclones-init`, and `sclones-stop` to $HOME/.local/bin
- Copy `sclones@.service`, `sclones@.timer`, and `sclones-shutdown@.service` to $HOME/.local/share/systemd/user

## Usage

Run `sclones-init` passing the local source and the remote destination as arguments. You can use either local directories or rclone remote directories. Currently tested only through SFTP.

To stop synching a directory, run `sclones-stop` using the same arguments as before. This doesn't delete anything, it just makes a last sync and disables the scheduled syncs. You can re-run `sclones-init` againt to remuse syncing.

## Uninstalling

Run `sclones-stop` for every directory you synced, and delete all the installed files.

## TODO

- Improve ignore file synchronization
- Replace local paths with default system ones
- Create AUR package
- (Maybe) Create installer
