#!/usr/bin/env bash

RCLONE_COMMAND=rclone

if [ "$#" -ne 2 ]; then
	printf "Source and destination required\n"
	exit 1
fi

source_dir="$1"
dest_dir="$2"

# TODO: Redo in a sh compatible way
sclonestest_list=("$source_dir"/.sclonestest-*)
[ "${#sclonestest_list[@]}" -ge 2 ] && printf "Multiple syncs to a local directory are not supported\n" && exit 1
sclonestest_file="${sclonestest_list[0]}"

if [ ! -f "$sclonestest_file" ]; then

	local_content=$($RCLONE_COMMAND lsf -R "$source_dir")
	remote_content=$($RCLONE_COMMAND lsf -R "$dest_dir")

	if [ -n "${local_content-unset}" ]; then
		if [ -n "${remote_content-unset}" ]; then
			printf "Neither the local nor the remote directories are empty. At least one must be empty before a sync can be safely setup\n"
			exit 1
		fi
	fi

	new_sclonestest_file="$source_dir/.sclonestest-$(uuidgen)"
	cat <<-"EOF" >"$new_sclonestest_file"
		THIS FILE IS USED TO CHECK THE INTEGRITY OF THE LOCAL AND REMOTE DIRECTORIES! DO NOT REMOVE!
	EOF

	if [ -z "${remote_content-unset}" ]; then
		cat <<-"EOF" >"$source_dir/.sclonesignore"
			# This file can exclude other files and folders from being synced from the local directory to the remote
			# Note that if a file already exists in the remote and is added to the exclusion list, it will be deleted

			# If a file is ignored in a local, it must be ignored in every local, otherwise unwanted exclusions might occur.
			# This file is thus synced to the remote prevent such issue

			# For syntax and examples, check here: https://rclone.org/filtering/
		EOF

		sclonesignore_md5=$(md5sum "$source_dir/.sclonesignore" | cut -d " " -f 1)
		printf '%s' "$sclonesignore_md5" >"$source_dir/.sclonesignore.md5"
	else
		$RCLONE_COMMAND cat "$dest_dir/.sclonesignore" >"$source_dir/.sclonesignore"
	fi

	$RCLONE_COMMAND bisync "$source_dir" "$dest_dir" \
		--resync-mode path2 \
		--max-lock 2m \
		--filters-file "$source_dir"/.sclonesignore \
		--filter "- /.sclonesbackup/**" --filter "+ /.sclonesignore" --filter "+ /.sclonesignore.md5" \
		--filter "+ /${new_sclonestest_file##*/}" --filter "- /.sclonestest-*" \
		--backup-dir1="$source_dir/.sclonesbackup" --backup-dir2="$dest_dir/.sclonesbackup" \
		--compare size,checksum \
		--verbose
else
	sclonesignore_previous_md5=$(cat "$source_dir/.sclonesignore.md5")
	sclonesignore_current_md5=$(md5sum "$source_dir/.sclonesignore" | cut -d " " -f 1)
	if [ "$sclonesignore_previous_md5" != "$sclonesignore_current_md5" ]; then
		printf '%s' "$sclonesignore_current_md5" >"$source_dir/.sclonesignore.md5"
	fi

	$RCLONE_COMMAND bisync "$source_dir" "$dest_dir" \
		--check-access --check-filename "${sclonestest_file##*/}" \
		--resilient --recover --max-lock 2m \
		--filters-file "$source_dir"/.sclonesignore \
		--filter "- /.sclonesbackup/**" --filter "+ /.sclonesignore" --filter "+ /.sclonesignore.md5" \
		--filter "+ /${sclonestest_file##*/}" --filter "- /.sclonestest-*" \
		--backup-dir1="$source_dir/.sclonesbackup" --backup-dir2="$dest_dir/.sclonesbackup" \
		--compare size,checksum --track-renames \
		--conflict-resolve path1 --conflict-suffix "{MacFriendlyTime}-remote-" \
		--verbose
fi
